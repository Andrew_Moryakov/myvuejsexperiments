import Vue from 'vue'
import App from './App.vue'
import App2 from './app2.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})

new Vue({
  el: '#app2',
  data:{
    value:0
  },
  components:{
    AppComponent: App2
  }
})
